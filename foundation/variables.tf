variable "domain" {
  type        = string
  description = "The domain to host the plum tree under e.g. theplumtreeapp.com"
}

variable "cognito_email_verified" {
  type        = bool
  description = "Boolean as towhether the manual step to verify the email cognito uses is verified"
  default     = false
}

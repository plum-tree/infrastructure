# Cognito

Creates the AWS Cognito pool and client for storing users and dealing with authentication.

# Cognito Email Verification

By default the module uses the `COGNITO_DEFAULT` email for sending emails (e.g. email verification links and auth codes).

Because it is a manual process to verify an email in AWS SES the modules does not use this by default (e.g. sender email as `noreply@${var.domain}`).

However the module does create the email identity and saves emails received to an S3 bucket. This is done in the same way as described at [How to verify an email address in SES which does not have an inbox][1].

Once the cognito module is applied for the first time you ca head to SES, resent the verification email and find the verification link in the S3 bucket `ses-email-${YOUR_ACCOUNT_ID}}`.

Then re-run the Terraform passing `cognito_email_verified` set to true which is used as the module `email_verified` var. The cognito pool is then updated to use the email with the domain of your choosing.

```shell
export TF_VAR_cognito_email_verified=true
terraform apply
```

https://aws.amazon.com/blogs/messaging-and-targeting/how-to-verify-an-email-address-in-ses-which-does-not-have-an-inbox/

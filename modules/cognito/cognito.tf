resource "aws_cognito_user_pool" "applicaiton_users" {
  name = "plumtree-users"

  # verification config
  auto_verified_attributes = ["email"]
  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "email"
    required                 = true
    string_attribute_constraints {
      max_length = "2048"
      min_length = "0"
    }
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }

  user_pool_add_ons {
    advanced_security_mode = "ENFORCED"
  }

  mfa_configuration = "OPTIONAL"

  software_token_mfa_configuration {
    enabled = true
  }

  admin_create_user_config {
    allow_admin_create_user_only = false
  }

  dynamic "email_configuration" {
    for_each = var.email_verified ? ["this"] : []

    content {
      email_sending_account  = "DEVELOPER"
      reply_to_email_address = aws_ses_email_identity.noreply.email
      source_arn             = aws_ses_email_identity.noreply.arn
    }
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name            = "plumtree-client"
  user_pool_id    = aws_cognito_user_pool.applicaiton_users.id
  generate_secret = true

  # configure hosted UI
  supported_identity_providers = ["COGNITO"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows = ["code"]
  allowed_oauth_scopes = ["email", "openid", "profile", "aws.cognito.signin.user.admin"]
  callback_urls = [
    "https://blue.${var.domain}/api/auth/callback",
    "https://green.${var.domain}/api/auth/callback",
    "https://${var.domain}/api/auth/callback",
    "http://localhost:8080/api/auth/callback",
  ]
  logout_urls = [
    "https://blue.${var.domain}/api/auth/logout",
    "https://green.${var.domain}/api/auth/logout",
    "https://${var.domain}/api/auth/logout",
    "http://localhost:8080/api/auth/logout",
  ]

  read_attributes = [
    # profile scope
    "birthdate",
    "nickname",
    "family_name",
    "gender",
    "given_name",
    "locale",
    "middle_name",
    "name",
    "picture",
    "preferred_username",
    "profile",
    "zoneinfo",
    "updated_at",
    "website",
    # email scope
    "email",
    "email_verified"
  ]
  write_attributes = [
    # profile scope
    "birthdate",
    "family_name",
    "gender",
    "given_name",
    "locale",
    "middle_name",
    "name",
    "nickname",
    "picture",
    "preferred_username",
    "profile",
    "zoneinfo",
    "updated_at",
    "website",
    "email"
  ]

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_cognito_user_pool_ui_customization" "styling" {
  client_id    = aws_cognito_user_pool_client.client.id
  user_pool_id = aws_cognito_user_pool_domain.main.user_pool_id
  css          = file("${path.module}/styles.css")
}

resource "aws_cognito_user_pool_domain" "main" {
  domain          = "auth.${var.domain}"
  certificate_arn = aws_acm_certificate.auth_certificate.arn
  user_pool_id    = aws_cognito_user_pool.applicaiton_users.id

  depends_on = [aws_acm_certificate_validation.auth_certificate]
}

resource "aws_acm_certificate" "auth_certificate" {
  provider          = aws.virginia
  domain_name       = "auth.${data.aws_route53_zone.primary.name}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "auth_cert_validation" {
  for_each = {
    for dvo in aws_acm_certificate.auth_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.primary.id
}

resource "aws_acm_certificate_validation" "auth_certificate" {
  provider                = aws.virginia
  certificate_arn         = aws_acm_certificate.auth_certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.auth_cert_validation : record.fqdn]
}

resource "aws_route53_record" "cognito_auth_custom_domain" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "auth.${data.aws_route53_zone.primary.name}"
  type    = "A"
  alias {
    name = aws_cognito_user_pool_domain.main.cloudfront_distribution_arn
    // The following zone id is CloudFront.
    // See https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-route53-aliastarget.html
    zone_id = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }

  depends_on = [aws_cognito_user_pool_domain.main]
}
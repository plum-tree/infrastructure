resource "aws_s3_bucket" "email_bucket" {
  bucket = "ses-email-${data.aws_caller_identity.current.account_id}"
  tags   = {
    Name        = "ses-email-${data.aws_caller_identity.current.account_id}"
    Description = "Bucket we forward SES email to for Cognito so we can verify the noreply email address"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "temp" {
  bucket = aws_s3_bucket.email_bucket.id

  rule {
    id     = "temp"
    status = "Enabled"

    expiration {
      days = 30
    }
  }
}

resource "aws_s3_bucket_policy" "allow_ses_emails" {
  bucket = aws_s3_bucket.email_bucket.id
  policy = data.aws_iam_policy_document.allow_ses_emails.json
}

data "aws_iam_policy_document" "allow_ses_emails" {
  statement {
    sid = "AllowSESPuts"

    principals {
      type        = "Service"
      identifiers = ["ses.amazonaws.com"]
    }

    actions = [
      "s3:PutObject",
    ]

    resources = [
      "${aws_s3_bucket.email_bucket.arn}/*",
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceAccount"

      values = [
        data.aws_caller_identity.current.account_id,
      ]
    }

    condition {
      test     = "StringLike"
      variable = "AWS:SourceArn"

      values = [
        "arn:aws:ses:*",
      ]
    }
  }
}

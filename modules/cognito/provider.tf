# Only ACM certs from us-east-1 work with cognito custom domains
provider "aws" {
  alias = "virginia"
  region = "us-east-1"
}

resource "aws_ses_email_identity" "noreply" {
  email = "noreply@${var.domain}"
}

# Verifying a custom email cognito should use is a manual step but the below
# should setup most of the work by verifying the domain for emails, setting up
# a MX record and forwarding emails to S3. Once provisioned you can verify the
# aws_ses_email_identity above by re-sending the verification email and finding
# the link in S3.
# See https://aws.amazon.com/blogs/messaging-and-targeting/how-to-verify-an-email-address-in-ses-which-does-not-have-an-inbox/

resource "aws_ses_domain_identity" "domain" {
  domain = var.domain
}

resource "aws_route53_record" "amazonses_verification_record" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "_amazonses.${aws_ses_domain_identity.domain.id}"
  type    = "TXT"
  ttl     = "600"
  records = [aws_ses_domain_identity.domain.verification_token]
}

resource "aws_ses_domain_identity_verification" "verification" {
  domain = aws_ses_domain_identity.domain.id

  depends_on = [aws_route53_record.amazonses_verification_record]
}

resource "aws_route53_record" "amazonses_mx_record" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = var.domain
  type    = "MX"
  ttl     = "60"
  records = ["10 inbound-smtp.eu-west-1.amazonaws.com"]
}

resource "aws_ses_receipt_rule" "ses_receiving" {
  name          = "ses-receiving"
  rule_set_name = "ses-receiving"
  recipients    = [aws_ses_email_identity.noreply.email]
  enabled       = true
  scan_enabled  = true

  s3_action {
    bucket_name = aws_s3_bucket.email_bucket.id
    position    = 1
  }
}
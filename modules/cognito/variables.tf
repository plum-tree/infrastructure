variable "domain" {
  type        = string
  description = "The application web domain"
}

variable "email_verified" {
  type        = bool
  description = "Boolean as to whether the manual step to verify the email cognito uses has been done"
  default     = false
}
resource "aws_dynamodb_table" "trees" {
  name           = "Trees"
  billing_mode   = "PROVISIONED"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "UserId"
  range_key      = "TreeId"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "TreeId"
    type = "S"
  }

  tags = {
    Name        = "Trees"
    Description = "Plum tree users tree records"
  }
}

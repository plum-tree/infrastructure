resource "aws_dynamodb_table" "published" {
  name           = "Published"
  billing_mode   = "PROVISIONED"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "TreeId"
  range_key      = "UserId"

  attribute {
    name = "TreeId"
    type = "S"
  }

  attribute {
    name = "UserId"
    type = "S"
  }

  tags = {
    Name        = "Published"
    Description = "Plum tree published trees and the trees people"
  }
}

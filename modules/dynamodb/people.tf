resource "aws_dynamodb_table" "people" {
  name           = "People"
  billing_mode   = "PROVISIONED"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "UserId"
  range_key      = "PersonId"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "PersonId"
    type = "S"
  }

  tags = {
    Name        = "People"
    Description = "Plum tree people records used in trees"
  }
}

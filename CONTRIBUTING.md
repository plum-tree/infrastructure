# Contributing

All contributions small or large are welcome. Even if it's just updating docs or making the code clearer without functional changes this helps others to contribute down the line.

> :see_no_evil: Please don't "Request Access" to the plum-tree project in GitLab (Developer requests will be rejected). Instead you should follow the instructions below and contribute using forks.

> :palm_tree: The plum tree uses [GitHub flow][1] as a branching strategy. The TLDR of this is create feature branches from `main` and when ready to review create merge requests back into the `main` branch.


1. Create a fork. There is a button at the top right that allows you to create your own fork (copy of the code) under your own GitLab user.
2. Clone your fork and create a feature branch for your change from `main`.
3. Push your changes back to your fork. GitLab CICD will run tests and let you deploy if you wish (you'll have to setup your own environment to deploy to).
4. Once happy with your changes open a merge request back to the plum tree project. Use the merge request templates to describe the change. It will be reviewed, tested and comments made.
5. Address any comments and once all is good the changes will be merged by a plum tree project member.


[1]: https://docs.github.com/en/get-started/quickstart/github-flow